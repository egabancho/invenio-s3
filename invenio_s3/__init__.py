# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Esteban J. G. Gabancho.
#
# Invenio-S3 is free software; you can redistribute it and/or modify it
# under the terms of the MIT License; see LICENSE file for more details.
"""S3 file storage support for Invenio."""

from __future__ import absolute_import, print_function

from .ext import InvenioS3
from .storage import S3FSFileStorage, s3fs_storage_factory
from .version import __version__

__all__ = (
    '__version__',
    'InvenioS3',
    'S3FSFileStorage',
    's3fs_storage_factory',
)
