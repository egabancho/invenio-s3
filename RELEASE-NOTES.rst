===================
 Invenio-S3 v0.1.0
===================

Invenio-S3 v0.1.0 was released on TBD, 2018.

About
-----

S3 file storage support for Invenio. 

*This is an experimental developer preview release.*

What's new
----------

- Initial public release.

Installation
------------

   $ pip install invenio-s3==0.1.0

Documentation
-------------

   https://invenio-s3.readthedocs.io/

Happy hacking and thanks for flying Invenio-S3.

| Invenio Development Team
|   Email: info@inveniosoftware.org
|   IRC: #invenio on irc.freenode.net
|   Twitter: https://twitter.com/inveniosoftware
|   GitHub: https://github.com/egabancho/invenio-s3
|   URL: http://inveniosoftware.org
