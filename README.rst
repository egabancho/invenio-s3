..
    Copyright (C) 2018 Esteban J. G. Gabancho.
    Invenio-S3 is free software; you can redistribute it and/or modify it
    under the terms of the MIT License; see LICENSE file for more details.

============
 Invenio-S3
============

.. image:: https://img.shields.io/travis/egabancho/invenio-s3.svg
        :target: https://travis-ci.org/egabancho/invenio-s3

.. image:: https://img.shields.io/coveralls/egabancho/invenio-s3.svg
        :target: https://coveralls.io/r/egabancho/invenio-s3

.. image:: https://img.shields.io/github/tag/egabancho/invenio-s3.svg
        :target: https://github.com/egabancho/invenio-s3/releases

.. image:: https://img.shields.io/pypi/dm/invenio-s3.svg
        :target: https://pypi.python.org/pypi/invenio-s3

.. image:: https://img.shields.io/github/license/egabancho/invenio-s3.svg
        :target: https://github.com/egabancho/invenio-s3/blob/master/LICENSE

S3 file storage support for Invenio. 

*This is an experimental developer preview release.*

TODO: Please provide feature overview of module

Further documentation is available on
https://invenio-s3.readthedocs.io/
